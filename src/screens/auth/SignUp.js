import React, { Component } from "react";

import {
    Text,
    SafeAreaView,
    Button,
    Image,
    Dimensions,
    View,
    TouchableOpacity
} from "react-native";
import { connect } from 'react-redux';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            urls: [
                'https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg',
                'https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-260nw-789676552.jpg',
                'https://mzjewel.com/wp-content/uploads/2019/11/2c1d5578da20ceabc4a28343769a5c91.jpg',
                'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg'
            ],
            index: 0
        };
    }

    _onPressButton() {
        this.props.navigation.push('SignIn');
    }


    render() {
        console.log(this.state.index);
        
        return (
            <SafeAreaView style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>

                <Image source={{ uri: this.state.urls[this.state.index] }} style={{ width: screenWidth, height: 300, resizeMode: 'contain' }}></Image>
                <View style={{ width: screenWidth, height: 70, justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View>
                    {this.state.index>0?<TouchableOpacity
                        style={{
                            width: 100, height: 50,
                            backgroundColor: '#cccccc', borderRadius: 25,
                            justifyContent: 'center', alignItems: 'center'
                        }}
                        onPress={()=>{this.setState({index:this.state.index-1})}}>
                        <Text>Previous</Text>
                    </TouchableOpacity>:null}
                    </View>

                    {this.state.index<this.state.urls.length-1?<TouchableOpacity
                        style={{
                            width: 100, height: 50,
                            backgroundColor: '#cccccc', borderRadius: 25,
                            justifyContent: 'center', alignItems: 'center'
                        }}
                        onPress={()=>this.setState({index:this.state.index+1})}>
                        <Text>Next</Text>
                    </TouchableOpacity>
                    :null}
                </View>

            </SafeAreaView>
        );
    }
}



export default connect(
    null,
    null
)(SignUp);
