import React, { Component } from "react";

import {
    Text,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Image,
    View,
    TextInput,
    ScrollView,
    FlatList,
    Dimensions,
    Alert
} from "react-native";
import { connect } from 'react-redux';
import { actions as appActions } from "../../actions/AppActions";
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
class SignIn extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            url:'',
            urls:[
                'https://helpx.adobe.com/content/dam/help/en/stock/how-to/visual-reverse-image-search/jcr_content/main-pars/image/visual-reverse-image-search-v2_intro.jpg',
                'https://image.shutterstock.com/image-photo/beautiful-water-drop-on-dandelion-260nw-789676552.jpg'
            ]
        };
    }

    insert() {

        let data = Object.assign([], this.state.urls);
        
            data.push(this.state.url);
        

        this.setState({ urls: data })
    }

    delete(index) {
        let data = Object.assign([], this.state.urls);
        data.splice(index, 1);
        this.setState({ urls: data })
    }

    
    render() {
        return (
            <SafeAreaView style={{ flex: 1, alignItems: "center" }}>
               <TouchableOpacity style={{
                    width: 50, height: 50,
                    backgroundColor: '#cbcb', borderRadius: 25,
                    position: 'absolute', bottom: 20, right: 20, zIndex: 99999, justifyContent: 'center', alignItems: 'center'
                }}
                    onPress={() => {
                        this.setState({ isVisible: true })
                    }}>
                    <Image source={require('../../assets/images/plus.png')} style={{ width: 15, height: 15, resizeMode: 'contain' }}></Image>

                </TouchableOpacity>

                <TouchableOpacity style={{
                    width: 100, height: 50,
                    backgroundColor: '#cccccc', borderRadius: 25,
                    position: 'absolute', bottom: 20, left: 20, zIndex: 99999, justifyContent: 'center', alignItems: 'center'
                }}
                    onPress={() => {
                        this.props.navigation.push('SignUp');
                    }}>
                    <Text>Next Page</Text>

                </TouchableOpacity>

                <View style={{ width: '100%', height: 50, borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TextInput
                        placeholder='Enter Image url'
                        value={this.state.url}
                        style={{ width: '80%', paddingLeft: 20 }}
                        onChangeText={(text) => {
                            this.setState({ url: text })
                        }}
                    >

                    </TextInput>
                    <TouchableOpacity style={{ width: 50, height: 50, backgroundColor: '#cccccc', justifyContent: 'center', alignItems: 'center' }}
                    onPress={() => this.insert()}>
                       <Text>Insert</Text>
                    </TouchableOpacity>
                </View>
                <ScrollView>

                    <FlatList
                        inverted={true}
                        data={this.state.urls}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity onPress={()=>{
                                Alert.alert(
                                    "Warning",
                                    "Sure want to delete?",
                                    [
                                      {
                                        text: "Cancel",
                                        onPress: () => console.log("Cancel Pressed"),
                                        style: "cancel"
                                      },
                                      { text: "OK", onPress: () => this.delete(index) }
                                    ],
                                    { cancelable: false }
                                  );
                                }}>
                                <Image source={{uri:item}} style={{width:screenWidth,height:300,resizeMode:'contain'}}></Image>
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item}
                    />
                </ScrollView>



            </SafeAreaView>
        );
    }
}





const mapDispatchToProps = dispatch => ({
    setUserDetails: (data) => dispatch(appActions.setUserDetails(data))
});



export default connect(
    null,
    mapDispatchToProps
)(SignIn);
