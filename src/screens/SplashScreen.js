import React, { Component } from "react";

import {
    Text,
    SafeAreaView,
    View,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    FlatList,
    Image
} from "react-native";
import Modal from 'react-native-modal';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default class SplashScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            search: '',
            list: ['react', 'Hello', 'world', 'native'],
            data: ['react', 'Hello', 'world', 'native'],
            isVisible: false,
            value: '',
            flag: false,
            itemIndex: 0
        };
    }

    componentDidMount() {

    }

    insert() {

        let data = Object.assign([], this.state.list);
        if (this.state.flag) {
            data[this.state.itemIndex] = this.state.value
        }
        else {
            data.push(this.state.value);
        }

        this.setState({ list: data,data, isVisible: false, flag: false, value: '' })
    }

    delete(index) {
        let data = Object.assign([], this.state.list);
        let data1 = Object.assign([], this.state.data);
        data.splice(index, 1);
        data1.splice(index, 1);
        this.setState({ list: data,data:data1 })
    }


    searchFilterFunction = text => {
        const newData = this.state.list.filter(item => {
          const itemData = `${item.toUpperCase()}`;
          const textData = text.toUpperCase();
          return itemData.includes(textData)
          //return itemData.indexOf(textData) > -1;
        });
        this.setState({ data: newData });
      };
    

    render() {
        console.log(this.state.list);

        return (
            <SafeAreaView style={{ flex: 1, alignItems: "center" }}>
                <Modal isVisible={this.state.isVisible}
                    animationIn="slideInLeft"
                    animationOut="slideOutRight">
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View style={{ width: screenWidth - 50, height: 200, backgroundColor: '#fff', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={{
                                width: 25, height: 25,
                                backgroundColor: '#cccccc', borderRadius: 13,
                                position: 'absolute', top: 10, right: 10, zIndex: 99999,
                                alignItems: 'center', justifyContent: 'center'
                            }}
                                onPress={() => this.setState({ isVisible: false, flag: false })}>
                                <Image source={require('../assets/images/close.png')} style={{ width: 15, height: 15, resizeMode: 'contain' }}></Image>
                            </TouchableOpacity>
                            <View style={{ width: '80%', height: 50, borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextInput
                                    placeholder='Enter text'
                                    value={this.state.value}
                                    style={{ width: '80%', paddingLeft: 20 }}
                                    onChangeText={(text) => {
                                        this.setState({ value: text })
                                    }}
                                >

                                </TextInput>
                                <TouchableOpacity style={{ width: 50, height: 50, backgroundColor: '#cccccc', alignItems: 'center', justifyContent: 'center' }}
                                    onPress={() => this.insert()}>
                                    <Image source={require('../assets/images/send.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }}></Image>
                                </TouchableOpacity>
                                
                            </View>
                        </View>

                    </View>
                </Modal>
                <TouchableOpacity style={{
                    width: 50, height: 50,
                    backgroundColor: '#cbcb', borderRadius: 25,
                    position: 'absolute', bottom: 20, right: 20, zIndex: 99999, justifyContent: 'center', alignItems: 'center'
                }}
                    onPress={() => {
                        this.setState({ isVisible: true })
                    }}>
                    <Image source={require('../assets/images/plus.png')} style={{ width: 15, height: 15, resizeMode: 'contain' }}></Image>

                </TouchableOpacity>

                <TouchableOpacity style={{
                    width: 100, height: 50,
                    backgroundColor: '#cccccc', borderRadius: 25,
                    position: 'absolute', bottom: 20, left: 20, zIndex: 99999, justifyContent: 'center', alignItems: 'center'
                }}
                    onPress={() => {
                        this.props.navigation.push('SignIn');
                    }}>
                    <Text>Next Page</Text>

                </TouchableOpacity>

                <View style={{ width: '100%', height: 50, borderWidth: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TextInput
                        placeholder='Search'
                        style={{ width: '80%', paddingLeft: 20 }}
                        onChangeText={(text) => {
                            this.searchFilterFunction(text)
                        }}
                    >

                    </TextInput>
                    <TouchableOpacity style={{ width: 50, height: 50, backgroundColor: '#cccccc', justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../assets/images/search.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }}></Image>

                    </TouchableOpacity>
                </View>
                <ScrollView>

                    <FlatList
                        inverted={true}
                        data={this.state.data}
                        renderItem={({ item, index }) =>
                            <View style={{ width: screenWidth, height: 50, borderBottomWidth: 1, flexDirection: 'row', alignItems: 'center', paddingLeft: 20, justifyContent: 'space-between' }}>

                                <Text>{item}</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity style={{ width: 50, height: 50, backgroundColor: '#cccccc', justifyContent: 'center', alignItems: 'center', marginRight: 1 }}
                                        onPress={() => {
                                            this.setState({ flag: true, itemIndex: index, isVisible: true,value:item })
                                        }}>
                                        <Image source={require('../assets/images/edit.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }}></Image>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ width: 50, height: 50, backgroundColor: '#cccccc', justifyContent: 'center', alignItems: 'center' }}
                                        onPress={() => {
                                            this.delete(index)
                                        }}>
                                        <Image source={require('../assets/images/trash.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }}></Image>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                        keyExtractor={item => item}
                    />
                </ScrollView>



            </SafeAreaView>
        );
    }
}



